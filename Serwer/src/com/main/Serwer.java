package com.main;

import org.gnet.packet.Packet;
import org.gnet.server.ClientModel;
import org.gnet.server.GNetServer;
import org.gnet.server.ServerEventListener;

public class Serwer {

	public static void main(String[] args) {
		final String host = "127.0.0.1";
		final int port = 43594;
		final GNetServer netserwer = new GNetServer(host, port);
		
		netserwer.addEventListener(new ServerEventListener() {
			
			@Override
			protected void packetReceived(ClientModel arg0, Packet arg1) {
			
			}
			
			@Override
			protected void errorMessage(String arg0) {
				
			}
			
			@Override
			protected void debugMessage(String arg0) {
				
			}
			
			@Override
			protected void clientDisconnected(ClientModel e) {
				System.out.println("Klient roz��czy� si� po��czenie z serwerem");
			}
			
			@Override
			protected void clientConnected(ClientModel e) {
				System.out.println("Klient nawi�za� po��czenie z serwerem"+e.getUuid());
			}
		});
		netserwer.bind();
		netserwer.start();
	}

}
